@extends('layouts.app')

@section('content')
        <h1>Post</h1>
        @if(count($posts) > 0)
                @foreach($posts as $posts)
                <div class='well'>
                    <h3><a href="/posts/{{$posts->id}}">{{$post->title}}</h3>
                    <small>Written on {{$post->created_at}}</small>
                </div>
                @endforeach
                {{$posts->links()}}
                @else
                    <p>No Posts Found</p>
                @endif
@endsection